# encoding: utf-8

class Page
  include Mongoid::Document

  field :number,    :type => Integer
  field :processed, :type => Boolean, :default => false

  has_and_belongs_to_many :states
  has_many :establishments

  def self.find_or_create(hash)
    criteria = Page.where({:number => hash[:number]})
    if criteria.count == 0
      return Page.create({ :number => hash[:number] })
    else
      return criteria.first
    end
  end

  # retorna true se conseguir criar algum estabelecimento pela pagina, ou entao false...
  def self.create_establishments(page, state)
      if page
          if page.search("//*[@class='lista_empresas']").count == 0
              return false
          else
              page.search("//*[@class='lista_empresas']").each do |empresa|
                  if empresa.search("td").count > 1
                      td    = empresa.search("td")[0]
                      img   = Url::BASE + td.search("img").first.attributes['src'].value

                      td    = empresa.search("td")[1]
                      name  = td.search("b").first.text
                      other = td.text
                      email = td.search("> a").find{|l| l.attributes['href'].value.include? "mailto" }.text rescue nil
                      site  = td.search("> a").find{|l| l.attributes['href'].value.match(/(^$)|(^((http|https):\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.*)?$)/ix) }.text rescue nil
                  else
                      td    = empresa.search("td")[0]
                      name  = td.search("b").first.text
                      other = td.text
                      email = td.search("> a").find{|l| l.attributes['href'].value.include? "mailto" }.text rescue nil
                      site  = td.search("> a").find{|l| l.attributes['href'].value.match(/(^$)|(^((http|https):\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.*)?$)/ix) }.text rescue nil
                  end

                  # print empresa
                  puts "Estado: " + state.colorize(:blue) + " - Nome: " + name.colorize(:green)
                  puts "name: " + name.colorize(:red) rescue nil
                  #puts "img: " + img.colorize(:red) rescue nil
                  puts "email: " + email.colorize(:red) rescue nil
                  #puts "site: " + site.colorize(:red) rescue nil
                  #puts "other: " + other.colorize(:red) rescue nil

                  Establishment.find_or_create({
                      :img => img,
                      :name => name,
                      :other => other,
                      :email => email,
                      :site => site
                  });
              end
              return true
          end
      else
          return false
      end        
  end
end
