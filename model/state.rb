# encoding: utf-8

class State
  include Mongoid::Document
  
  field :initials,  :type => String
  field :processed, :type => Boolean, :default => false

  has_and_belongs_to_many :pages

  def self.find_or_create(hash)
  	criteria = State.where({ :initials => hash[:initials] })
    if criteria.count == 0
    	return State.create({ :initials => hash[:initials] })
    else
    	return criteria.first
    end
  end

  def self.initialize_states
  	out = []
  	['rj', 'sp', 'mg', 'rs', 'ba', 'es', 'mt', 'ms', 'to', 'go', 'rs', 'pr', 'sc', 'ac', 'al', 'ap', 'am', 'ce', 'df', 'se', 'ma', 'pb', 'pa', 'pe', 'pi', 'rn', 'ro', 'rr']
  	.each do |initials|      
  		state = State.find_or_create({ :initials => initials })
  		if !state.processed
  			out << state
  		end
  	end

  	return out
  end
end
