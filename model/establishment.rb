# encoding: utf-8

class Establishment
  include Mongoid::Document

  field :name,           :type => String
  field :img,            :type => String
  field :other,          :type => String
  field :email,          :type => String
  field :site,           :type => String

  belongs_to :page

  def self.find_or_create(hash)
    criteria = Establishment.where({ :name => hash[:name], :other => hash[:other], 
                                  :img => hash[:img], :email => hash[:email], :site => hash[:site] })
    
    if criteria.count > 0
      return criteria.first
    else
      return Establishment.create({:name => hash[:name], :other => hash[:other], :img => hash[:img], 
                                  :email => hash[:email], :site => hash[:site], :page => Page.find(hash[:page]) })
    end
  end
end
