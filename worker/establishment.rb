# encoding: utf-8

module Worker
	class Establishment
		include Sidekiq::Worker
		#sidekiq_options :queue => :critical

		def perform(hash)
			pg = Page.find hash['page']
		  	state = State.find hash['state']
		  	page = Url.get hash['url']

		  	puts "Processando pagina: " + pg.number.to_s + " -------------------------------------------------------------------------------------------------------------"
			puts "Estado: " + state.colorize(:blue) + " - Page: " + pg.number.to_s.colorize(:green)

	  		if page
				page.search("//*[@class='lista_empresas']").each do |empresa|
					if empresa.search("td").count > 1
				  		td    = empresa.search("td")[0]
				  		img   = Url::BASE + td.search("img").first.attributes['src'].value

				  		td    = empresa.search("td")[1]
				  		name  = td.search("b").first.text
				  		other = td.text
				  		email = td.search("> a").find{|l| l.attributes['href'].value.include? "mailto" }.text rescue nil
				  		site  = td.search("> a").find{|l| l.attributes['href'].value.match(/(^$)|(^((http|https):\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.*)?$)/ix) }.text rescue nil
					else
				  		td    = empresa.search("td")[0]
				  		name  = td.search("b").first.text
				  		other = td.text
				  		email = td.search("> a").find{|l| l.attributes['href'].value.include? "mailto" }.text rescue nil
				  		site  = td.search("> a").find{|l| l.attributes['href'].value.match(/(^$)|(^((http|https):\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.*)?$)/ix) }.text rescue nil
					end

					# print empresa
					puts "Estado: " + state.colorize(:blue) + " - Nome: " + name.colorize(:green)
					puts "Estado: " + state.colorize(:blue) + " - Email: " + name.colorize(:green)

					Establishment.find_or_create({
					  :img => img,
					  :name => name,
					  :other => other,
					  :email => email,
					  :site => site,
					  :page => pg.id
					});
				end				
	  		end

			pg.processed = true
			state.processed = true if state.pages.where(:processed => false).count == 0

			pg.save
			state.save

			puts "------------------------------------------------------------------------------------------------------------- Pagina processada"
	  	end
	end
end