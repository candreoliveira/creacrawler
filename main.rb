# encoding: utf-8

require 'bundler'

# Carrega as gems definidas no Gemfile
Bundler.require

# Carrega os arquivos necessários para executar o programa
require './model'
#require './worker'
require './url'

# Configura o mongodb
Mongoid.configure do |config|
  config.master = Mongo::Connection.new("localhost", 27017, :pool_size => 5, :pool_timeout => 5).db("crea_crawler")
end

# If your client is single-threaded, we just need a single connection in our Redis connection pool
#Sidekiq.configure_client do |config|
#  config.redis = { :namespace => 'x', :size => 1 }
#end

# Sidekiq server is multi-threaded so our Redis connection pool size defaults to concurrency (-c)
#Sidekiq.configure_server do |config|
#  config.redis = { :namespace => 'x' }
#end

# passar argumento para continuar e nao destruir tudo
if ARGV.include? "destroy_all"
	Establishment.destroy_all
	State.destroy_all
	Page.destroy_all
elsif ARGV.include? "reprocess_all"
	Page.all.each do |p|
		p.processed = false
		p.save		
	end
	
	State.all.each do |s|
		s.processed = false
		s.save
	end
end

# Cria todos os estados e retorna um array
State.initialize_states.each do |state|
	puts "Processando estado: " + state.initials + " -------------------------------------------------------------------------------------------------------------"	
	# passar opcao=empresasrj para funcionar no rj
	#index = 1
	#continue = true

	# paginacao
	# while continue do
	# 	index = index + 1
	# 	page = Url.get(Url::SEARCH + "uf=" + state + "&resultados=40&opcao=empresasrj&pagina="+ index.to_s)
	# 	continue = Url.get_establishment(page, state)
	# end

	# acessar a primeira pagina
	page = Url.get(Url.address(state.initials, 1))

	# acessar ultima pagina
	if page

		# link ultima
		# if page.search("//*[@class='barra_navegacao']/a").last.text.downcase.strip.unaccent == "ultima"
		href = page.search("//*[@class='barra_navegacao']/a").last.attributes['href'].value

		# pegar o numero da ultima pagina
		number = href.gsub(/.*&pagina=/, "").to_i

		# criar e colocar as paginas na fila para processamento
		(number).times do |i|
			pg = Page.find_or_create({ :number => (i+1) })
			pg.states << state if !pg.states.include? state
			state.pages << pg if !state.pages.include? pg
			
			# salvar objetos
			pg.save

			#Worker::Establishment.perform_async({ :url => Url.address(state.initials, pg.number), :page => pg.id, :state => state.id }) if !pg.processed
			Stalker.enqueue('establishment.worker', :hash => { :url => Url.address(state.initials, pg.number), :page => pg.id, :state => state.id }) if !pg.processed
		end
	else
		state.processed = true
	end
	state.save
	
	puts "Inicie os workers...".colorize(:red)	
	puts ""

	j=0
	while Page.where(:processed => false).count > 6000
		5.times do |i|
			j = j + 1
			print "Aguardando o processamento de algumas páginas para continuar... ".colorize(:red) + j.to_s.colorize(:red) + "\r"
			sleep(1)
		end		
	end

	puts "------------------------------------------------------------------------------------------------------------- Estado " + state.initials + " processado"
end

puts ""

j=0
while State.where(:processed => false).count > 0
	5.times do |i|
		j = j + 1
		print "Aguardando o processamento terminar... ".colorize(:red) + j.to_s.colorize(:red) + "\r"
		sleep(1)
	end
end

exit