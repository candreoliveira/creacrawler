class Url
    BASE = "http://www.ebge.com.br/crea/"
    SEARCH = "http://www.ebge.com.br/crea/pesquisa.aspx?"
    
	def self.get(url)
		# Inicializa o Mechanize
        agent               = Mechanize.new
        agent.max_history   = 1
        agent.read_timeout  = 120
        agent.user_agent    = Url.user_agent
    
        # Carrega a url
        begin
            page = agent.get(url)

            # Força o encoding da página para utf-8
            page.encoding = "utf-8"
    
            # Caso o kekanto tenha nos pego na protecao deles, sair do programa e renovar o ip
            raise "Crawler::Protection" if page.body.size == 0

            # Retorna a página consultada
            return page
        rescue
            return nil
        end
	end

    def self.address(state, page)
        return Url::SEARCH + "uf=" + state.to_s + "&resultados=40&opcao=empresasrj&pagina=" + page.to_s
    end

    def self.user_agent
        return "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11"
    end
end